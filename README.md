![Hoppr Docs Banner](static/repo-media/hoppr-docs-banner.png)

Hoppr.dev utilizes the [Docusaurus 2](https://tutorial.docusaurus.io/docs/intro) framework.
For simple documentation edits, use the `Edit this page` link at the bottom of a live page.  For more complex changes, the Docusaurus tutorial above and some basic React experience will help.

### Installation

```
$ yarn
```

### Local Development

```
$ yarn start
```

This command starts a local development server at `localhost:5000`. Most changes are hot-reloaded without having to restart the server.

### Docs Best Practices

1. To edit docs for the **next** (upcoming) Hoppr release, work from the [base /docs folder](https://gitlab.com/hoppr/hoppr-docs/-/tree/main/docs)
2. To edit docs for **current/past** Hoppr releases, edit the specific [/versioned_docs](https://gitlab.com/hoppr/hoppr-docs/-/tree/main/versioned_docs) and [/versioned_sidebars](https://gitlab.com/hoppr/hoppr-docs/-/tree/main/versioned_sidebars) files
3. [Use on-disk paths for all internal links](https://docusaurus.io/docs/versioning#link-docs-by-file-paths) (reason - Docusaurus auto-generates these paths across versions & also checks URLs)
4. [Create index pages for categories](https://docusaurus.io/docs/sidebar/items#category-doc-link) especially if you will link to them ([example](https://gitlab.com/hoppr/hoppr-docs/-/blob/main/docs/using-hoppr/ecosystem-plugins/_category_.json#L5))
5. Prefer Markdown over HTML/JSX to improve Algolia search results

### Build & Test Links

```
$ yarn build docusaurus.config.js
```

### Freezing a Docs Release

When a Hoppr version is released, freeze a set of docs for that version.  Example for `1.7.x`

```
yarn run docusaurus docs:version 1.7.x
```

Check that the build & links are valid and make an MR.