---
sidebar_position: 4
title: Sbom
---

# Sbom

A software bill of materials (sbom) declares the inventory of components used to build a software artifact such as a software application. Components in the SBOMs define **"what"** gets transferred. SBOM files are referenced by a `Manifest` YAML file to allow multiple deliverables to be combined into a single product.

## CycloneDX SBOMs

[CycloneDX](https://cyclonedx.org/) standard is the SBOM format we're using to define `Software Bill of Materials` (`sboms`).

### Reference

- [Capabilities](https://cyclonedx.org/capabilities/)
- [Use Cases and Examples](https://cyclonedx.org/use-cases/)
- [Guides and Resources](https://cyclonedx.org/guides/)
- [Guiding Principles](https://cyclonedx.org/about/guiding-principles/)
- [Specification Overview](https://cyclonedx.org/specification/overview/)
- [JSON Specification](https://cyclonedx.org/docs/1.5/json/)

### SBOM Generation Tools

There are a number of tools that can generate a CycloneDX SBOM for your project, the [CycloneDX Tool Center](https://cyclonedx.org/tool-center/) is a great place to start looking.

### Component Scope

Scope is an *optional* field within a component that sets the desired scope of that component. Hoppr uses this field to understand how to handle the various components found in the provided sbom(s)

According to the [CycloneDX specification](https://cyclonedx.org/docs/1.5/json/#components_items_scope) Scope can be set to three options:

- **Required**:
  - The default if scope not specified for a given component. Meaning if a components scope is not specified in the sbom, Hoppr will treat that component as *required* and will fail if that component fails to be collected. Even if the desired collector plugin is not included in the transfer.yml.
- **Optional**:
  - Hoppr will attempt to collect this component via the appropriate collector. Currently handled the same as 'required' (described above).
- **Excluded**:
  - No attempt to collect this component will be made. Hoppr does not send this component to any collector. The job output will document the excluded items for visibility.

Example Component with Scope field:

```json
{
    "type": "library",
    "name": "go-gitlab",
    "version": "0.81.0",
    "purl": "pkg:golang/github.com/xanzy/go-gitlab@v0.81.0",
    "scope": "excluded"
}
```

### Delta Sbom

Delta Sbom is a plugin to remove SBOM components that are specified by a *previously* defined `manifest.yml` or `tar` bundle. This plugin will take an input of an `manifest.yml` or `tar` bundle, compare it to the previous, and create a delta sbom. It checks for updates and if the component has been updated, will deliver.
For example, take 2 bundles. Bundle 2 has everything with the exact same versions from Bundle 1, except 4 new containers. These 4 new containers, also now known as deltas, would create the delta sbom.

Using the `-pd` or `--previous-delivery` flag represents the previous delivery for the path to manifest or tar bundles.
