---
sidebar_position: 6
---

# Hoppr CycloneDX Taxonomy

## Hoppr Namespace Taxonomy

Context: [What is CycloneDX Property Taxonomy?](https://github.com/CycloneDX/cyclonedx-property-taxonomy)

| Namespace | Description | Administered By | Taxonomy |
| --- | --- | --- | --- |
| `hoppr:collection` | Namespace for properties specific to Hoppr collections | Lockheed Martin Hoppr | `hoppr:repository` Namespace Taxonomy |
| `hoppr:repository` | Namespace for properties specific to Hoppr repositories | Lockheed Martin Hoppr | `hoppr:repository` Namespace Taxonomy |
| `hoppr:schema` | Namespace for properties specific to Hoppr input file schemas | Lockheed Martin Hoppr | `hoppr:schema` Namespace Taxonomy |

## `hoppr:collection` Namespace Taxonomy

| Namespace             | Description                                                       |
| --------------------- | ----------------------------------------------------------------- |
| `hoppr:collection:directory`  | The directory (relative to `collect_root_dir`) into which an artifact has been copied |
| `hoppr:collection:plugin`  | The class name of the plugin used to collect an artifact |
| `hoppr:collection:repository`  | The repository from which an artifact has been collected |
| `hoppr:collection:timetag`  | The UTC time at which an artifact was copied |

## `hoppr:repository` Namespace Taxonomy

| Namespace             | Description                                                       |
| --------------------- | ----------------------------------------------------------------- |
| `hoppr:repository:component_search_sequence`  | The ordered list of repositories used to find a component |

## `hoppr:schema` Namespace Taxonomy

| Namespace             | Description                                                       |
| --------------------- | ----------------------------------------------------------------- |
| `hoppr:schema:manifest_version`  | Manifest file schema version |
| `hoppr:schema:transfer_version`  | Transfer file schema version |
| `hoppr:schema:credentials_version`  | Credentials file schema version |
