---
sidebar_position: 1
---

# Installation

## Two Requirements

:::note Req #1 - Python

__Python 3.10__ or higher is required to run Hoppr.  Instructions to download Python are [available here](https://www.python.org/downloads/)

:::



:::note Req #2 - System Dependencies

Hoppr plugins often utilize underlying system applications.  This means that some plugins will only run
on certain Operating Systems.  For example, the `DNF` and `YUM` collectors generally require a RHEL-based platform.  Some examples:

  * ```collect_dnf_plugin``` requires __dnf__
  * ```collect_docker_plugin``` requires __skopeo__
  * ```collect_git_plugin``` requires __git__
  * ```collect_helm_plugin``` requires __helm__
  * ```collect_maven_plugin``` requires __mvn__
  * ```collect_pypi_plugin``` requires __pip__
  * ```collect_yum_plugin``` requires __yumdownloader__

:::

## OS Configuration

### Ubuntu

```bash
apt-get install --assume-yes python3.10 python3.10-venv python3-pip maven nodejs npm git skopeo

ln -sf /usr/bin/pip3 /usr/bin/pip
ln -sf /usr/bin/python3 /usr/bin/python
```

### Rocky

:::tip

Rocky Linux requires a build of Python 3.10, you'll need to install and configure it before following the next steps.

:::

```bash
# Start Docker
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

yum-config-manager --enable powertools

yum install docker-ce docker-ce-cli containerd.io docker-compose-plugin --assumeyes --allowerasing

yum install skopeo --assumeyes --allowerasing

```

### Alpine

```bash
apk add --no-cache python3-dev git maven nodejs npm skopeo
```

## Setup Pip3, Python3 and Helm

```bash
ln -sf /usr/bin/pip3 /usr/bin/pip
ln -sf /usr/bin/python3 /usr/bin/python

curl -L https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash

```

## Install Hoppr CLI

Hoppr is available on [pypi.org](https://pypi.org/project/hoppr/).  You can install Hoppr simply by running the following command

:::tip

On some systems you must use the `pip3` command rather than `pip`

:::

```bash
# Install virtual environment 
pip install virtualenv 

# Instantiate virtual environment 
python -m venv env 

# Source virtual environment 
source env/bin/activate 

# Install hoppr

## Python Run

pip install hoppr
```

or

```bash
pip3 install hoppr
```

You can verify that Hoppr is installed by executing `hopctl version`:

```bash
$ hopctl version
Hoppr Framework Version: {{ hoppr.version }}
Python Version: 3.10.4
```

## Docker Run

You can run hoppr with the `hopctl` docker image

```bash
$ docker run hoppr/hopctl version
Hoppr Framework Version: {{ hoppr.version }}
Python Version: 3.10.4
```
