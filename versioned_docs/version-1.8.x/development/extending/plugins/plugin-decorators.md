---
title: Plugin Decorators
sidebar_position: 2
---

The Hoppr framework provides two Python decorators to simplify the development of plugin processes.

### 1. @hoppr_process

The `@hoppr_process` decorator handles several tasks that any implementation of `pre_stage_process`, `process_component`, or `post_stage_process` must perform.  These include:

- Creation of a logger (accessible via the `get_logger()` method) to which information may be written regarding the execution of the process.  This is an "in-memory" logger, which is copied to stdout at the completion of the processes, thereby preventing interleaving log messages from multiple processes running in parallel.

- Logging of certain standard events: start and completion of the process, run time, and result.

- For `process_component` (or any method that passes an argument of type `Component`), check that the process is appropriate to the PURL type of the component.

- At process completion, close the logger, and dump its contents to stdout.

### 2. @hoppr_rerunner

The `@hoppr_rerunner` decorator runs a process repeatedly, up to the value `max_attempts` specified in the Context, waiting for `retry_wait_seconds` (also from the Context) seconds between attempts.

### 3. Combining the decorators

The two decorators can (and often will be) combined for a single method.  We recommend that the order be:

```python
    @hoppr_process
    @hoppr_rerunner
    def process_component(....)
```

:::caution

Reversing the decorator order will still work, but will be slightly less efficient - creating and destroying loggers and logging data for each attempt.

:::
