import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

type FeatureItem = {
  title: string;
  image: string;
  description: JSX.Element;
};

const FeatureList: FeatureItem[] = [
  {
    title: 'SBOM-Defined Processing',
    image: require('@site/static/img/feature-sbom-proc-01.png').default,
    description: (
      <>
        Hoppr leverages the industry-standard CycloneDX format for Software Bill of Materials (SBOM) processing.
        Combined with Hoppr's simple manifest format, teams can share their dependencies to represent the entire software supply chain as code.
      </>
    ),
  },
  {
    title: 'Repeatable Bundles',
    image: require('@site/static/img/feature-bundles-02.png').default,
    description: (
      <>
        Collect and bundle your software dependencies with rock-solid confidence.
        Hoppr builds the bundle, you decide where it goes - an airgapped network, 
        production deployment, and beyond. Attestable and repeatable bundles, every single time.
      </>
    ),
  },
  {
    title: 'Open Source',
    image: require('@site/static/img/feature-open-source-03.png').default,
    description: (
      <>
        We're completely open source, licensed under MIT, and community friendly. 
        Built with a plugin architecture, Hoppr enables users to extend its SBOM-processing capabilities
        through their own plugins and algorithms. Come join our project!
      </>
    ),
  },
];

function Feature({title, image, description}: FeatureItem) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <img src={image} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures(): JSX.Element {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
