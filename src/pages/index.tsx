import React from 'react';
import clsx from 'clsx';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import Layout from '@theme/Layout';
import HomepageFeatures from '@site/src/components/HomepageFeatures';

import styles from './index.module.css';
import { ParallaxProvider, ParallaxBanner, ParallaxBannerLayer } from 'react-scroll-parallax';

function HomepageHeader() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <ParallaxProvider>
      <header style={{ backgroundColor: '#375cfd' }} className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h2>Collect, process, & bundle your software supply chain</h2>
          <div className={styles.buttons}>
            <Link
              className="button button--secondary button--lg"
              to="/docs/intro">
              Get Started - 5min ⏱️
            </Link>
          </div>
        </div>        
      </header>
      <ParallaxBanner style={{ aspectRatio: '2 / 1' }}>
        <ParallaxBannerLayer style={{ backgroundPosition: 'top' }} image="img/hoppr-parallax/640h/0-sky-640.png" />
        <ParallaxBannerLayer style={{ backgroundPosition: 'top' }} image="img/hoppr-parallax/640h/01-cloud-640.png" translateX={['-10','10']} translateY={['10','10']}/>
        <ParallaxBannerLayer style={{ backgroundPosition: 'top' }} image="img/hoppr-parallax/640h/03-more-clouds-640.png" translateX={['-10','10']}/>
        <ParallaxBannerLayer style={{ backgroundPosition: 'top' }} image="img/hoppr-parallax/640h/04-packages-cloud-640.png" translateY={['-200px','100px']} />
        <ParallaxBannerLayer style={{ backgroundPosition: 'top' }} image="img/hoppr-parallax/640h/05-more-packages-640.png" speed={-14} />
        <ParallaxBannerLayer style={{ backgroundPosition: 'top' }} image="img/hoppr-parallax/640h/06-airplane-640.png" translateX={['-200', '200']} />
        <ParallaxBannerLayer style={{ backgroundPosition: 'top' }} image="img/hoppr-parallax/640h/02-land-640.png" />
      </ParallaxBanner>
    </ParallaxProvider>
  );
}

export default function Home(): JSX.Element {
  const {siteConfig} = useDocusaurusContext();
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Description will go into a meta tag in <head />">
      <HomepageHeader />
      <main>
        <HomepageFeatures />
      </main>
    </Layout>
  );
}
